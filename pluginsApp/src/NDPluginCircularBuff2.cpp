/*
 * NDPluginCircularBuff2.cpp
 *
 * Based on NDPluginCircularBuff.cpp by Alan Greer.
 *
 * Scope style triggered image recording with ability
 * to control waveform PV update rate
 *
 * Author: Hinko Kocevar
 *
 * Created November 21, 2021
 */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#include <epicsThread.h>
#include <epicsMath.h>
#include <iocsh.h>

#include "NDPluginCircularBuff2.h"

#include <epicsExport.h>

static const char *driverName="NDPluginCircularBuff2";

#define DEFAULT_TRIGGER_CALC "0"

// print to IOC shell
#define __PRN(MASK, TEXT, pasynUser, fmt, ...) do { \
    if (pasynUser) \
        asynPrint(pasynUser, MASK, "%s: " TEXT " " fmt "\n", driverName, ##__VA_ARGS__); \
    else \
        asynPrint(pasynUserSelf, MASK, "%s: " TEXT " " fmt "\n", driverName, ##__VA_ARGS__); \
} while (0)
#define ERR(pasynUser, fmt, ...)  __PRN(ASYN_TRACE_ERROR, "[ERROR]", pasynUser, fmt, ##__VA_ARGS__)
#define WARN(pasynUser, fmt, ...) __PRN(ASYN_TRACE_WARNING, "[WARN]", pasynUser, fmt, ##__VA_ARGS__)
#define INFO(pasynUser, fmt, ...) __PRN(ASYN_TRACEIO_DRIVER, "[INFO]", pasynUser, fmt, ##__VA_ARGS__)
#define FLOW(pasynUser, fmt, ...) __PRN(ASYN_TRACE_FLOW, "[FLOW]", pasynUser, fmt, ##__VA_ARGS__)
// set asyn error message
#define SETERR(pasynUser, fmt, ...) do { \
    epicsSnprintf(pasynUser->errorMessage, pasynUser->errorMessageSize, \
        "%s:%s:" fmt "\n", driverName, __func__, ##__VA_ARGS__); \
} while (0)

/** Callback function that is called by the NDArray driver with new NDArray data.
  * Stores the number of pre-trigger images prior to the trigger in a ring buffer.
  * Once the trigger has been received stores the number of post-trigger buffers
  * and then exposes the buffers.
  * \param[in] pArray  The NDArray from the callback.
  */
void NDPluginCircularBuff2::processCallbacks(NDArray *pArray) {
    int scopeControl, preCount, postCount, currentImage, currentPostCount, softTrigger;
    NDArray *pArrayCpy = NULL;
    NDArrayInfo arrayInfo;
    int triggered = 0;

    // Call the base class method
    NDPluginDriver::beginProcessCallbacks(pArray);

    pArray->getInfo(&arrayInfo);

    // Retrieve the running state
    getIntegerParam(NDCircBuffControl,      &scopeControl);
    getIntegerParam(NDCircBuffPreTrigger,   &preCount);
    getIntegerParam(NDCircBuffPostTrigger,  &postCount);
    getIntegerParam(NDCircBuffCurrentImage, &currentImage);
    getIntegerParam(NDCircBuffPostCount,    &currentPostCount);
    getIntegerParam(NDCircBuffSoftTrigger,  &softTrigger);
    getIntegerParam(NDCircBuffTriggered,    &triggered);

    // Are we running?
    if (scopeControl) {
        // First copy the buffer into our buffer pool so we can release the resource on the driver
        pArrayCpy = this->pNDArrayPool->copy(pArray, NULL, 1);

        if (pArrayCpy) {
            // Have we detected a trigger event yet?
            if (!triggered) {
                // No trigger so add the NDArray to the pre-trigger ring
                pOldArray_ = preBuffer_->addToEnd(pArrayCpy);
                // If we overwrote an existing array in the ring, release it here
                if (pOldArray_) {
                    pOldArray_->release();
                    pOldArray_ = NULL;
                }
                // Set the size
                setIntegerParam(NDCircBuffCurrentImage,  preBuffer_->size());
                if (preBuffer_->size() == preCount) {
                    setStringParam(NDCircBuffStatus,
                            preCount ? "Pre-buffer Wrapping" : "Dropping frames");
                }
                FLOW(pasynUserSelf, "pre buffer filling %d / %d", preBuffer_->size(), preCount);
            } else {
                // Trigger detected
                // Start making frames available if trigger has occured
                setStringParam(NDCircBuffStatus, "Post-buffer filling");
                FLOW(pasynUserSelf, "post buffer filling %d / %d", postBuffer_->size(), postCount);

                currentPostCount++;
                setIntegerParam(NDCircBuffPostCount,  currentPostCount);

                pOldArray_ = postBuffer_->addToEnd(pArrayCpy);
                if (pOldArray_) {
                    ERR(pasynUserSelf, "this should not happen");
                }
            }

            // Stop recording once we have reached the post-trigger count, wait for a restart
            if (currentPostCount >= postCount) {
                FLOW(pasynUserSelf, "acquisition done!");
                // setIntegerParam(NDCircBuffTriggered, 0);
                setIntegerParam(NDCircBuffControl, 0);
                setStringParam(NDCircBuffStatus, "Acquisition Completed");
            }
        } else {
            ERR(pasynUserSelf, "pArray NULL, failed to copy the data");
        }
    } else {
        // Currently do nothing
    }

    callParamCallbacks();
}


void NDPluginCircularBuff2::bufferControl(int value) {
    int preCount, postCount;

    if (value == 1) {
        INFO(pasynUserSelf, "starting acquisition");
        // If the control is turned on then create our new ring buffer
        getIntegerParam(NDCircBuffPreTrigger,  &preCount);
        if (preBuffer_) {
            delete preBuffer_;
        }
        preBuffer_ = new NDArrayRing(preCount);
        getIntegerParam(NDCircBuffPostTrigger,  &postCount);
        if (postBuffer_) {
            delete postBuffer_;
        }
        postBuffer_ = new NDArrayRing(postCount);
        if (pOldArray_) {
            pOldArray_->release();
        }
        pOldArray_ = NULL;

        previousTrigger_ = 0;

        // Set the status to buffer filling
        setIntegerParam(NDCircBuffSoftTrigger, 0);
        setIntegerParam(NDCircBuffTriggered, 0);
        setIntegerParam(NDCircBuffPostCount, 0);
        setIntegerParam(NDCircBuffFlushPreCount, 0);
        setIntegerParam(NDCircBuffFlushPostCount, 0);
        setStringParam(NDCircBuffStatus,
                preCount ? "Pre-buffer filling" : "Dropping frames");
    } else {
        INFO(pasynUserSelf, "stopping acquisition/flushing");
        // Control is turned off, before we have finished
        // Set the trigger value off, reset counter
        setIntegerParam(NDCircBuffSoftTrigger, 0);
        setIntegerParam(NDCircBuffTriggered, 0);
        setIntegerParam(NDCircBuffCurrentImage, 0);
        setIntegerParam(NDCircBuffPostCount, 0);
        setIntegerParam(NDCircBuffFlushPreCount, 0);
        setIntegerParam(NDCircBuffFlushPostCount, 0);
        setStringParam(NDCircBuffStatus, "Acquisition Stopped");
        stage_ = 0;
        // wake up the thread waiting for on demand event;
        // thread will not do any work since stage is 0
        epicsEventSignal(onDemandEventId_);
    }

    // Set the parameter in the parameter library.
    setIntegerParam(NDCircBuffControl, value);
}

/** Called when asyn clients call pasynInt32->write().
  * This function performs actions for some parameters.
  * For all parameters it sets the value in the parameter library and calls any registered callbacks..
  * \param[in] pasynUser pasynUser structure that encodes the reason and address.
  * \param[in] value Value to write.
  */
asynStatus NDPluginCircularBuff2::writeInt32(asynUser *pasynUser, epicsInt32 value) {
    int function = pasynUser->reason;
    asynStatus status = asynSuccess;

    if (function == NDCircBuffControl) {
        bufferControl(value);

        epicsInt32 scopeControl;
        getIntegerParam(NDCircBuffControl, &scopeControl);
        int triggered;
        getIntegerParam(NDCircBuffTriggered, &triggered);
        int trigger;
        getIntegerParam(NDCircBuffSoftTrigger, &trigger);
        INFO(pasynUser, "scopeControl %d, triggered %d, trigger %d", scopeControl, triggered, trigger);

    }  else if (function == NDCircBuffSoftTrigger) {
        // Set the parameter in the parameter library.
        status = (asynStatus) setIntegerParam(function, value);

        epicsInt32 scopeControl;
        getIntegerParam(NDCircBuffControl, &scopeControl);
        epicsInt32 triggered;
        getIntegerParam(NDCircBuffTriggered, &triggered);
        INFO(pasynUser, "scopeControl %d, triggered %d, trigger %d", scopeControl, triggered, value);

        if (scopeControl && !triggered && value) {
            // Set a soft trigger
            setIntegerParam(NDCircBuffTriggered, 1);
            INFO(pasynUser, "signaling start SOFT flush event");
            epicsEventSignal(flushEventId_);
        }

    }  else if (function == NDCircBuffPreTrigger) {
        epicsInt32 scopeControl;
        getIntegerParam(NDCircBuffControl, &scopeControl);

        if (scopeControl) {
            setStringParam(NDCircBuffStatus, "Stop acquisition to set pre-count");
        } else if (value > (maxBuffers_ - 1)) {
            // The value of pretrigger should not exceed max buffers
            setStringParam(NDCircBuffStatus, "Pre-count too high");
        } else if (value < 0) {
            setStringParam(NDCircBuffStatus, "Invalid pre-count value");
        } else {
            // Set the parameter in the parameter library.
            status = (asynStatus) setIntegerParam(function, value);
        }

    }  else if (function == NDCircBuffPostTrigger) {
        epicsInt32 scopeControl;
        getIntegerParam(NDCircBuffControl, &scopeControl);

        if (scopeControl) {
            setStringParam(NDCircBuffStatus, "Stop acquisition to set post-count");
        } else if (value > (maxBuffers_ - 1)) {
            // The value of pretrigger should not exceed max buffers
            setStringParam(NDCircBuffStatus, "Post-count too high");
        } else if (value < 0) {
            setStringParam(NDCircBuffStatus, "Invalid post-count value");
        } else {
            // Set the parameter in the parameter library.
            status = (asynStatus) setIntegerParam(function, value);
        }

    }  else if (function == NDCircBuffFlushOnDemand) {
        if (value == 1) {
            status = (asynStatus) setIntegerParam(function, 1);
            if (stage_ != 0) {
                INFO(pasynUser, "signaling on demand flush event");
                epicsEventSignal(onDemandEventId_);
            } else {
                // set busy record to done
                status = (asynStatus) setIntegerParam(function, 0);
            }
        } else {
            // set busy record to done
            status = (asynStatus) setIntegerParam(function, 0);
        }

    } else {
        // Set the parameter in the parameter library.
        status = (asynStatus) setIntegerParam(function, value);

        // If this parameter belongs to a base class call its method
        if (function < FIRST_NDPLUGIN_CIRC_BUFF_PARAM)
            status = NDPluginDriver::writeInt32(pasynUser, value);
    }

    // Do callbacks so higher layers see any changes
    status = (asynStatus) callParamCallbacks();

    if (status) {
        ERR(pasynUser, "status=%d, function=%d, value=%d", status, function, value);
        SETERR(pasynUser, "status=%d, function=%d, value=%d", status, function, value);
    } else {
        INFO(pasynUser, "function=%d, value=%d", function, value);
    }
    return status;
}

/** Called when asyn clients call pasynOctet->write().
  * This function performs actions for some parameters, including AttributesFile.
  * For all parameters it sets the value in the parameter library and calls any registered callbacks..
  * \param[in] pasynUser pasynUser structure that encodes the reason and address.
  * \param[in] value Address of the string to write.
  * \param[in] nChars Number of characters to write.
  * \param[out] nActual Number of characters actually written.
  */
asynStatus NDPluginCircularBuff2::writeOctet(asynUser *pasynUser, const char *value, size_t nChars, size_t *nActual) {
    int addr=0;
    int function = pasynUser->reason;
    asynStatus status = asynSuccess;
    short postfixError;

    status = getAddress(pasynUser, &addr); if (status != asynSuccess) return(status);
    // Set the parameter in the parameter library.
    status = (asynStatus)setStringParam(addr, function, (char *)value);
    if (status != asynSuccess) return(status);

    if (function == NDCircBuffTriggerCalc) {
        if (nChars > sizeof(triggerCalcInfix_)) nChars = sizeof(triggerCalcInfix_);
        // If the input string is empty then use a value of "0", otherwise there is an error
        if ((value == 0) || (strlen(value) == 0)) {
            strcpy(triggerCalcInfix_, DEFAULT_TRIGGER_CALC);
            setStringParam(NDCircBuffTriggerCalc, DEFAULT_TRIGGER_CALC);
        } else {
            // Issue: specified bound 100 equals destination size [-Wstringop-truncation]
            // strncpy(triggerCalcInfix_, value, sizeof(triggerCalcInfix_));
            // Solution: nChars is checked and corrected above for this purpose, right?
            strncpy(triggerCalcInfix_, value, nChars);
        }
        status = (asynStatus)postfix(triggerCalcInfix_, triggerCalcPostfix_, &postfixError);
        if (status) {
            ERR(pasynUser, "error processing infix expression=%s, error=%s",
                triggerCalcInfix_, calcErrorStr(postfixError));
        }
    }

    else if (function < FIRST_NDPLUGIN_CIRC_BUFF_PARAM) {
        // If this parameter belongs to a base class call its method
        status = NDPluginDriver::writeOctet(pasynUser, value, nChars, nActual);
    }

    // Do callbacks so higher layers see any changes
    callParamCallbacks(addr, addr);

    if (status) {
        ERR(pasynUser, "status=%d, function=%d, value=%s", status, function, value);
        SETERR(pasynUser, "status=%d, function=%d, value=%s", status, function, value);
    } else {
        INFO(pasynUser, "function=%d, value=%s", function, value);
    }
    *nActual = nChars;
    return status;
}

static void flushingTaskC(void *drvPvt) {
    NDPluginCircularBuff2 *pPvt = (NDPluginCircularBuff2 *)drvPvt;
    pPvt->flushingTask();
}

// This thread starts sending the accumulated arrays.
void NDPluginCircularBuff2::flushingTask() {
    int status = asynSuccess;
    double delay;
    int currentPreCount, preCount;
    int currentPostCount, postCount;
    int flushMode, rearm;

    stage_ = 0;
    currentPreCount = 0;
    currentPostCount = 0;

    this->lock();
    while (1) {
        callParamCallbacks();

        if (stage_ == 0) {
            getIntegerParam(NDCircBuffRearm, &rearm);
            // if rearm is set restart the buffer filling
            if (rearm) {
                bufferControl(0);
                bufferControl(1);
            }

            // Do callbacks so higher layers see any changes
            callParamCallbacks();

            INFO(pasynUserSelf, "waiting for start flush event");
            this->unlock();
            status = epicsEventWait(flushEventId_);
            this->lock();
            INFO(pasynUserSelf, "flush event received (status %d)", status);

            stage_ = 1;
            currentPreCount = 0;
            currentPostCount = 0;
            setIntegerParam(NDCircBuffFlushPreCount, 0);
            setIntegerParam(NDCircBuffFlushPostCount, 0);
            getIntegerParam(NDCircBuffPreTrigger, &preCount);
            getIntegerParam(NDCircBuffPostTrigger, &postCount);

            // consume any old on demand events at this point
            epicsEventTryWait(onDemandEventId_);

            callParamCallbacks();
        }

        // we are here if flushing was requested
        FLOW(pasynUserSelf, "flushing, stage %d, pre %d / %d, post %d / %d", stage_,
            currentPreCount, preCount, currentPostCount, postCount);
        setStringParam(NDCircBuffStatus, "Flushing");

        getIntegerParam(NDCircBuffFlushMode, &flushMode);
        if (flushMode) {
            // on demand flushing; write to FlushOnDemand PV
            INFO(pasynUserSelf, "waiting for on demand flush event..");
            this->unlock();
            status = epicsEventWait(onDemandEventId_);
            this->lock();
            INFO(pasynUserSelf, "on demand flush event received (status %d)", status);
            callParamCallbacks();
        } else {
            getDoubleParam(NDCircBuffFlushDelay, &delay);
            // periodic flushing; sleep for FlushDelay PV value amount of seconds
            this->unlock();
            epicsThreadSleep(delay);
            this->lock();
        }

        if (stage_ == 1) {
            FLOW(pasynUserSelf, "stage 1 pre buffer size %d", preBuffer_->size());
            if (! preBuffer_) {
                stage_ = 3;
            } else if (preBuffer_->size() == 0) {
                stage_ = 3;
            } else {
                doCallbacksGenericPointer(preBuffer_->readFromStart(), NDArrayData, 0);
                currentPreCount++;
                setIntegerParam(NDCircBuffFlushPreCount, currentPreCount);
                FLOW(pasynUserSelf, "stage 1 pre buffer %d / %d", currentPreCount, preCount);
                stage_ = 2;
                // signal the busy record done
                setIntegerParam(NDCircBuffFlushOnDemand, 0);
            }
        } else if (stage_ == 2) {
            if (! preBuffer_) {
                stage_ = 3;
            } else if (preBuffer_->size() == 0) {
                stage_ = 3;
            } else {
                if (preBuffer_->hasNext()) {
                    doCallbacksGenericPointer(preBuffer_->readNext(), NDArrayData, 0);
                    currentPreCount++;
                    setIntegerParam(NDCircBuffFlushPreCount, currentPreCount);
                    FLOW(pasynUserSelf, "stage 2a pre buffer %d / %d", currentPreCount, preCount);
                    // signal the busy record done
                    setIntegerParam(NDCircBuffFlushOnDemand, 0);
                }
                if (currentPreCount == preBuffer_->size()) {
                    preBuffer_->clear();
                    stage_ = 3;
                    FLOW(pasynUserSelf, "stage 2b pre buffer %d / %d", currentPreCount, preCount);
                }
            }
        } else if (stage_ == 3) {
            FLOW(pasynUserSelf, "stage 3 post buffer size %d", postBuffer_->size());
            if (! postBuffer_) {
                stage_ = 4;
            } else if (postBuffer_->size() == 0) {
                stage_ = 4;
            } else if (postBuffer_->size() > 0) {
                doCallbacksGenericPointer(postBuffer_->readFromStart(), NDArrayData, 0);
                currentPostCount++;
                setIntegerParam(NDCircBuffFlushPostCount, currentPostCount);
                FLOW(pasynUserSelf, "stage 3 post buffer %d / %d", currentPostCount, postCount);
                stage_ = 4;
                // signal the busy record done
                setIntegerParam(NDCircBuffFlushOnDemand, 0);
            }
        } else if (stage_ == 4) {
            if (postBuffer_->hasNext()) {
                doCallbacksGenericPointer(postBuffer_->readNext(), NDArrayData, 0);
                currentPostCount++;
                setIntegerParam(NDCircBuffFlushPostCount, currentPostCount);
                FLOW(pasynUserSelf, "stage 4 post buffer %d / %d", currentPostCount, postCount);
                // signal the busy record done
                setIntegerParam(NDCircBuffFlushOnDemand, 0);
            }
            if (currentPostCount >= postCount) {
                postBuffer_->clear();
                stage_ = 0;
                delay = 0.0;
                INFO(pasynUserSelf, "flushing done");
                setStringParam(NDCircBuffStatus, "Flushing completed");
                setIntegerParam(NDCircBuffTriggered, 0);
            }
        } else {
            ERR(pasynUserSelf, "should not be here, flushing stage %d ?!", stage_);
            stage_ = 0;
            setIntegerParam(NDCircBuffTriggered, 0);
            // signal the busy record done
            setIntegerParam(NDCircBuffFlushOnDemand, 0);
        }
    }
}

/** Constructor for NDPluginCircularBuff; most parameters are simply passed to NDPluginDriver::NDPluginDriver.
  * After calling the base class constructor this method sets reasonable default values for all of the
  * parameters.
  * \param[in] portName The name of the asyn port driver to be created.
  * \param[in] queueSize The number of NDArrays that the input queue for this plugin can hold when
  *            NDPluginDriverBlockingCallbacks=0.  Larger queues can decrease the number of dropped arrays,
  *            at the expense of more NDArray buffers being allocated from the underlying driver's NDArrayPool.
  * \param[in] blockingCallbacks Initial setting for the NDPluginDriverBlockingCallbacks flag.
  *            0=callbacks are queued and executed by the callback thread; 1 callbacks execute in the thread
  *            of the driver doing the callbacks.
  * \param[in] NDArrayPort Name of asyn port driver for initial source of NDArray callbacks.
  * \param[in] NDArrayAddr asyn port driver address for initial source of NDArray callbacks.
  * \param[in] maxBuffers The maximum number of NDArray buffers that the NDArrayPool for this driver is
  *            allowed to allocate. Set this to -1 to allow an unlimited number of buffers.
  * \param[in] maxMemory The maximum amount of memory that the NDArrayPool for this driver is
  *            allowed to allocate. Set this to -1 to allow an unlimited amount of memory.
  * \param[in] priority The thread priority for the asyn port driver thread if ASYN_CANBLOCK is set in asynFlags.
  * \param[in] stackSize The stack size for the asyn port driver thread if ASYN_CANBLOCK is set in asynFlags.
  */
NDPluginCircularBuff2::NDPluginCircularBuff2(const char *portName, int queueSize, int blockingCallbacks,
                         const char *NDArrayPort, int NDArrayAddr,
                         int maxBuffers, size_t maxMemory,
                         int priority, int stackSize)
    : NDPluginDriver(portName, queueSize, blockingCallbacks,
                NDArrayPort, NDArrayAddr, 1, maxBuffers, maxMemory,
                asynInt32ArrayMask | asynFloat64ArrayMask | asynGenericPointerMask,
                asynInt32ArrayMask | asynFloat64ArrayMask | asynGenericPointerMask,
                0, 1, priority, stackSize, 1), pOldArray_(NULL)
{
    preBuffer_ = NULL;
    postBuffer_ = NULL;
    int status = asynSuccess;

    maxBuffers_ = maxBuffers;

    createParam(NDCircBuffControlString,            asynParamInt32,      &NDCircBuffControl);
    createParam(NDCircBuffStatusString,             asynParamOctet,      &NDCircBuffStatus);
    createParam(NDCircBuffTriggerAString,           asynParamOctet,      &NDCircBuffTriggerA);
    createParam(NDCircBuffTriggerBString,           asynParamOctet,      &NDCircBuffTriggerB);
    createParam(NDCircBuffTriggerAValString,        asynParamFloat64,    &NDCircBuffTriggerAVal);
    createParam(NDCircBuffTriggerBValString,        asynParamFloat64,    &NDCircBuffTriggerBVal);
    createParam(NDCircBuffTriggerCalcString,        asynParamOctet,      &NDCircBuffTriggerCalc);
    createParam(NDCircBuffTriggerCalcValString,     asynParamFloat64,    &NDCircBuffTriggerCalcVal);
    createParam(NDCircBuffPreTriggerString,         asynParamInt32,      &NDCircBuffPreTrigger);
    createParam(NDCircBuffPostTriggerString,        asynParamInt32,      &NDCircBuffPostTrigger);
    createParam(NDCircBuffCurrentImageString,       asynParamInt32,      &NDCircBuffCurrentImage);
    createParam(NDCircBuffPostCountString,          asynParamInt32,      &NDCircBuffPostCount);
    createParam(NDCircBuffSoftTriggerString,        asynParamInt32,      &NDCircBuffSoftTrigger);
    createParam(NDCircBuffTriggeredString,          asynParamInt32,      &NDCircBuffTriggered);
    createParam(NDCircBuffFlushOnSoftTrigString,    asynParamInt32,      &NDCircBuffFlushOnSoftTrig);
    createParam(NDCircBuffFlushDelayString,         asynParamFloat64,    &NDCircBuffFlushDelay);
    createParam(NDCircBuffFlushModeString,          asynParamInt32,      &NDCircBuffFlushMode);
    createParam(NDCircBuffFlushOnDemandString,      asynParamInt32,      &NDCircBuffFlushOnDemand);
    createParam(NDCircBuffFlushPreCountString,      asynParamInt32,      &NDCircBuffFlushPreCount);
    createParam(NDCircBuffFlushPostCountString,     asynParamInt32,      &NDCircBuffFlushPostCount);
    createParam(NDCircBuffRearmString,              asynParamInt32,      &NDCircBuffRearm);

    // Set the plugin type string
    setStringParam(NDPluginDriverPluginType, "NDPluginCircularBuff2");

    // Set the status to idle
    setStringParam(NDCircBuffStatus, "Idle");

    // Init the current frame count to zero
    setIntegerParam(NDCircBuffCurrentImage, 0);
    setIntegerParam(NDCircBuffPostCount, 0);

    // Init the scope control to off
    setIntegerParam(NDCircBuffControl, 0);

    // Init the pre and post count to 10
    setIntegerParam(NDCircBuffPreTrigger, 10);
    setIntegerParam(NDCircBuffPostTrigger, 10);

    setIntegerParam(NDCircBuffFlushOnSoftTrig, 0);

    // Array flush delay
    setDoubleParam(NDCircBuffFlushDelay, 1.0);

    // Use periodic flush mode
    setIntegerParam(NDCircBuffFlushMode, 0);
    setIntegerParam(NDCircBuffFlushOnDemand, 0);

    // Flush counters
    setIntegerParam(NDCircBuffFlushPreCount, 0);
    setIntegerParam(NDCircBuffFlushPostCount, 0);

    setIntegerParam(NDCircBuffRearm, 0);

    // Enable ArrayCallbacks.
    // This plugin currently ignores this setting and always does callbacks,
    // so make the setting reflect the behavior
    setIntegerParam(NDArrayCallbacks, 1);

    // Create the epicsEvent for signaling to the flushing task when to start
    flushEventId_ = epicsEventCreate(epicsEventEmpty);
    if (!flushEventId_) {
        ERR(pasynUserSelf, "epicsEventCreate failure for flush event");
        return;
    }

    // Create the epicsEvent for issuing on demand flush trigger
    onDemandEventId_ = epicsEventCreate(epicsEventEmpty);
    if (!onDemandEventId_) {
        ERR(pasynUserSelf, "epicsEventCreate failure for on demand flush event");
        return;
    }

    // Create the thread that flushes the images
    status = (epicsThreadCreate("flushingTask",
            epicsThreadPriorityMedium, epicsThreadGetStackSize(epicsThreadStackMedium),
            (EPICSTHREADFUNC)flushingTaskC, this) == NULL);
    if (status) {
        ERR(pasynUserSelf, "epicsThreadCreate failure for flushing task");
        return;
    }

    // Try to connect to the array port
    connectToArrayPort();
}

// Configuration command
extern "C" int NDCircularBuff2Configure(const char *portName, int queueSize, int blockingCallbacks,
                                        const char *NDArrayPort, int NDArrayAddr, int maxBuffers,
                                        size_t maxMemory) {
        NDPluginCircularBuff2 *pPlugin = new NDPluginCircularBuff2(portName, queueSize, blockingCallbacks,
                                        NDArrayPort, NDArrayAddr, maxBuffers, maxMemory, 0, 2000000);
        return pPlugin->start();
}

// EPICS iocsh shell commands
static const iocshArg initArg0 = { "portName",iocshArgString};
static const iocshArg initArg1 = { "frame queue size",iocshArgInt};
static const iocshArg initArg2 = { "blocking callbacks",iocshArgInt};
static const iocshArg initArg3 = { "NDArrayPort",iocshArgString};
static const iocshArg initArg4 = { "NDArrayAddr",iocshArgInt};
static const iocshArg initArg5 = { "maxBuffers",iocshArgInt};
static const iocshArg initArg6 = { "maxMemory",iocshArgInt};
static const iocshArg * const initArgs[] = {&initArg0,
                                            &initArg1,
                                            &initArg2,
                                            &initArg3,
                                            &initArg4,
                                            &initArg5,
                                            &initArg6};
static const iocshFuncDef initFuncDef = {"NDCircularBuff2Configure",7,initArgs};
static void initCallFunc(const iocshArgBuf *args) {
    NDCircularBuff2Configure(args[0].sval, args[1].ival, args[2].ival,
                    args[3].sval, args[4].ival, args[5].ival, args[6].ival);
}

extern "C" void NDCircularBuff2Register(void) {
    iocshRegister(&initFuncDef,initCallFunc);
}

extern "C" {
epicsExportRegistrar(NDCircularBuff2Register);
}
