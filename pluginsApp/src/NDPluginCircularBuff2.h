#ifndef NDPluginCircularBuff2_H
#define NDPluginCircularBuff2_H

#include <epicsTypes.h>
#include <postfix.h>

#include "NDPluginDriver.h"
#include "NDArrayRing.h"

/* Param definitions */
#define NDCircBuffControlString             "CIRC_BUFF_CONTROL"               /* (asynInt32,        r/w) Run scope? */
#define NDCircBuffStatusString              "CIRC_BUFF_STATUS"                /* (asynOctetRead,    r/o) Scope status */
#define NDCircBuffTriggerAString            "CIRC_BUFF_TRIGGER_A"             /* (asynOctetWrite,   r/w) Trigger A attribute name */
#define NDCircBuffTriggerBString            "CIRC_BUFF_TRIGGER_B"             /* (asynOctetWrite,   r/w) Trigger B attribute name */
#define NDCircBuffTriggerAValString         "CIRC_BUFF_TRIGGER_A_VAL"         /* (asynFloat64,      r/o) Trigger A value */
#define NDCircBuffTriggerBValString         "CIRC_BUFF_TRIGGER_B_VAL"         /* (asynFloat64,      r/o) Trigger B value */
#define NDCircBuffTriggerCalcString         "CIRC_BUFF_TRIGGER_CALC"          /* (asynOctetWrite,   r/w) Trigger calculation expression */
#define NDCircBuffTriggerCalcValString      "CIRC_BUFF_TRIGGER_CALC_VAL"      /* (asynFloat64,      r/o) Trigger calculation value */
#define NDCircBuffPreTriggerString          "CIRC_BUFF_PRE_TRIGGER"           /* (asynInt32,        r/w) Number of pre-trigger images */
#define NDCircBuffPostTriggerString         "CIRC_BUFF_POST_TRIGGER"          /* (asynInt32,        r/w) Number of post-trigger images */
#define NDCircBuffCurrentImageString        "CIRC_BUFF_CURRENT_IMAGE"         /* (asynInt32,        r/o) Number of the current image */
#define NDCircBuffPostCountString           "CIRC_BUFF_POST_COUNT"            /* (asynInt32,        r/o) Number of the current post count image */
#define NDCircBuffSoftTriggerString         "CIRC_BUFF_SOFT_TRIGGER"          /* (asynInt32,        r/w) Force a soft trigger */
#define NDCircBuffTriggeredString           "CIRC_BUFF_TRIGGERED"             /* (asynInt32,        r/o) Have we had a trigger event */
#define NDCircBuffFlushOnSoftTrigString     "CIRC_BUFF_FLUSH_ON_SOFTTRIGGER"  /* (asynInt32,        r/w) Flush buffer immediatelly when software trigger obtained */
#define NDCircBuffFlushDelayString          "CIRC_BUFF_FLUSH_DELAY"           /* (asynFloat64,      r/w) Delay between array flushed */
#define NDCircBuffFlushModeString           "CIRC_BUFF_FLUSH_MODE"            /* (asynInt32,        r/w) Flush trigger mode can be periodical or on demand */
#define NDCircBuffFlushOnDemandString       "CIRC_BUFF_FLUSH_ON_DEMAND"       /* (asynInt32,        r/w) Issue on demand flush trigger */
#define NDCircBuffFlushPreCountString       "CIRC_BUFF_FLUSH_PRE_COUNT"       /* (asynInt32,        r/o) Number of the current flush pre count image */
#define NDCircBuffFlushPostCountString      "CIRC_BUFF_FLUSH_POST_COUNT"      /* (asynInt32,        r/o) Number of the current flush post count image */
#define NDCircBuffRearmString               "CIRC_BUFF_REARM"                 /* (asynInt32,        r/w) Re-arm buffering after flushing */


/** Performs a scope like capture.  Records a quantity
  * of pre-trigger and post-trigger images
  */
class NDPluginCircularBuff2 : public NDPluginDriver {
public:
    NDPluginCircularBuff2(const char *portName, int queueSize, int blockingCallbacks,
                 const char *NDArrayPort, int NDArrayAddr,
                 int maxBuffers, size_t maxMemory,
                 int priority, int stackSize);
    /* These methods override the virtual methods in the base class */
    void processCallbacks(NDArray *pArray);
    asynStatus writeInt32(asynUser *pasynUser, epicsInt32 value);
    asynStatus writeOctet(asynUser *pasynUser, const char *value, size_t nChars, size_t *nActual);

    //template <typename epicsType> asynStatus doProcessCircularBuffT(NDArray *pArray);
    //asynStatus doProcessCircularBuff(NDArray *pArray);
    void flushingTask(); /**< Should be private, but gets called from C, so must be public */

protected:
    int NDCircBuffControl;
    #define FIRST_NDPLUGIN_CIRC_BUFF_PARAM NDCircBuffControl
    /* Scope */
    int NDCircBuffStatus;
    int NDCircBuffTriggerA;
    int NDCircBuffTriggerB;
    int NDCircBuffTriggerAVal;
    int NDCircBuffTriggerBVal;
    int NDCircBuffTriggerCalc;
    int NDCircBuffTriggerCalcVal;
    int NDCircBuffPreTrigger;
    int NDCircBuffPostTrigger;
    int NDCircBuffCurrentImage;
    int NDCircBuffPostCount;
    int NDCircBuffSoftTrigger;
    int NDCircBuffTriggered;
    int NDCircBuffFlushOnSoftTrig;
    int NDCircBuffFlushDelay;
    int NDCircBuffFlushMode;
    int NDCircBuffFlushOnDemand;
    int NDCircBuffFlushPreCount;
    int NDCircBuffFlushPostCount;
    int NDCircBuffRearm;

private:
    void bufferControl(int value);
    NDArrayRing *preBuffer_;
    NDArray *pOldArray_;
    int previousTrigger_;
    int maxBuffers_;
    char triggerCalcInfix_[MAX_INFIX_SIZE];
    char triggerCalcPostfix_[MAX_POSTFIX_SIZE];
    double triggerCalcArgs_[CALCPERFORM_NARGS];
    int flushing_;
    epicsEventId flushEventId_;
    NDArrayRing *postBuffer_;
    int stage_;
    epicsEventId onDemandEventId_;
};

#endif

